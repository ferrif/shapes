#!/usr/bin/env fish

# image features
set iwidth  10
set iheight 10
set bgcolor white
set stroke_color black
set stroke_width 1
set stroke_antialias 0

# shape features
set size_min 2
set px_length (math $iwidth \* $iheight - 1)

# naming convention for output files
#
# <shape>_<id>_<x ref>_<y ref>_<size>_<feature>.png
#
# where <shape> <x ref>, <y ref> and <size> are
#
# circle     center_x       center_y   radius
# cross      center_x       center_y   length_of_the_half_arm
# triangle   center_x       center_y   length_of_the_half_side   
#
# The triangle is equilateral, and the center is equidistant from the three vertices

function draw
        set output $argv[-1]
        set cmd $argv[1..-2]
        convert -size {$iwidth}x{$iheight} xc:white \
                        -fill $bgcolor \
                        -stroke $stroke_color \
                        -strokewidth $stroke_width \
                        +antialias \
                        -draw "$cmd" $output
end


function gen_circle
        set shape 'circle'
        set ix $argv[1]
        set iy $argv[2]
        set size $argv[3]
        set cnt 0
        if test (count $argv) -ge 4; set cnt $argv[4]; end
        set odir $shape
        set ofile (printf '%s_%06d_%04d_%04d_%04d.png' $shape $cnt $ix $iy $size)
        set cmd (printf 'circle %d,%d %d,%d' $ix $iy (math $ix + $size) $iy)
        draw $cmd $odir/$ofile
end


function gen_cross
        set shape 'cross'
        set ix $argv[1]
        set iy $argv[2]
        set size $argv[3]
        set cnt 0
        if test (count $argv) -ge 4; set cnt $argv[4]; end
        set odir $shape
        set ofile (printf '%s_%06d_%04d_%04d_%04d.png' $shape $cnt $ix $iy $size)
        set cmd (printf 'line %d,%d %d,%d line %d,%d %d,%d\n' \
                (math $ix - $size) $iy (math $ix + $size) $iy \
                $ix (math $iy - $size) $ix (math $iy + $size))
        draw $cmd $odir/$ofile
end


function gen_triangle
        set shape 'triangle'
        set ix $argv[1]
        set iy $argv[2]
        set size $argv[3]
        set cnt 0
        if test (count $argv) -ge 4; set cnt $argv[4]; end
        set odir $shape
        set ofile (printf '%s_%06d_%04d_%04d_%04d.png' $shape $cnt $ix $iy $size)
        set tmp_y (math --scale=0 $iy + $size \* 0.8660254037844386)
        set cmd (printf 'polygon %d,%d %d,%d %d,%d\n' \
                (math $ix - $size) $tmp_y (math $ix + $size) $tmp_y $ix (math --scale=0 $iy - $size \* 0.8660254037844386))
        draw $cmd $odir/$ofile
end


function random_ix_iy_size
        while true
                set px (random 0 $px_length)
                set ix (math $px % $iwidth)
                set iy (math --scale=0 $px / $iwidth)
                set max_size_x (if test $ix -lt (math $iwidth / 2); echo $ix; else; echo (math $iwidth - $ix); end)
                set max_size_y (if test $iy -lt (math $iheight / 2); echo $iy; else; echo (math $iheight - $iy); end)
                set max_size (if test $max_size_x -lt $max_size_y; echo $max_size_x; else; echo $max_size_y; end)
                #echo $px $ix $iy $max_size_x $max_size_y $max_size
                if test $max_size -le $size_min; continue; end
                set size (random $size_min $max_size)
                echo $ix $iy $size
                return
        end
end


mkdir -p circle cross triangle

set cnt 0
for i in (seq 1 10000)
        set v (string split " " (random_ix_iy_size))
        gen_cross  $v[1] $v[2] $v[3] $i
        set v (string split " " (random_ix_iy_size))
        gen_circle $v[1] $v[2] $v[3] $i
        set v (string split " " (random_ix_iy_size))
        gen_triangle $v[1] $v[2] $v[3] $i
        echo $i
end
exit
